﻿namespace ToolTipDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblX = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.lblText = new System.Windows.Forms.Label();
            this.txtX = new System.Windows.Forms.TextBox();
            this.txtY = new System.Windows.Forms.TextBox();
            this.txtText = new System.Windows.Forms.TextBox();
            this.btnCreateToolTip = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnHideAllToolTips = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDestroyAllToolTips = new System.Windows.Forms.Button();
            this.btnEditToolTip = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(12, 9);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(17, 13);
            this.lblX.TabIndex = 0;
            this.lblX.Text = "X:";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(12, 47);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(17, 13);
            this.lblY.TabIndex = 1;
            this.lblY.Text = "Y:";
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.Location = new System.Drawing.Point(12, 81);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(31, 13);
            this.lblText.TabIndex = 2;
            this.lblText.Text = "Text:";
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(63, 6);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(100, 20);
            this.txtX.TabIndex = 3;
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(63, 44);
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(100, 20);
            this.txtY.TabIndex = 4;
            // 
            // txtText
            // 
            this.txtText.Location = new System.Drawing.Point(63, 81);
            this.txtText.Multiline = true;
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(209, 139);
            this.txtText.TabIndex = 5;
            // 
            // btnCreateToolTip
            // 
            this.btnCreateToolTip.Location = new System.Drawing.Point(169, 245);
            this.btnCreateToolTip.Name = "btnCreateToolTip";
            this.btnCreateToolTip.Size = new System.Drawing.Size(103, 23);
            this.btnCreateToolTip.TabIndex = 6;
            this.btnCreateToolTip.Text = "Create Tool Tip";
            this.btnCreateToolTip.UseVisualStyleBackColor = true;
            this.btnCreateToolTip.Click += new System.EventHandler(this.btnCreateToolTip_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 286);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "-------------------------------------------------------------------";
            // 
            // btnHideAllToolTips
            // 
            this.btnHideAllToolTips.Location = new System.Drawing.Point(15, 311);
            this.btnHideAllToolTips.Name = "btnHideAllToolTips";
            this.btnHideAllToolTips.Size = new System.Drawing.Size(103, 23);
            this.btnHideAllToolTips.TabIndex = 8;
            this.btnHideAllToolTips.Text = "Hide all Tool Tips";
            this.btnHideAllToolTips.UseVisualStyleBackColor = true;
            this.btnHideAllToolTips.Click += new System.EventHandler(this.btnHideAllToolTips_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(170, 311);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Show all Tool Tips";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 348);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(208, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "-------------------------------------------------------------------";
            // 
            // btnDestroyAllToolTips
            // 
            this.btnDestroyAllToolTips.Location = new System.Drawing.Point(151, 382);
            this.btnDestroyAllToolTips.Name = "btnDestroyAllToolTips";
            this.btnDestroyAllToolTips.Size = new System.Drawing.Size(121, 23);
            this.btnDestroyAllToolTips.TabIndex = 11;
            this.btnDestroyAllToolTips.Text = "Destroy all Tool Tips";
            this.btnDestroyAllToolTips.UseVisualStyleBackColor = true;
            this.btnDestroyAllToolTips.Click += new System.EventHandler(this.btnDestroyAllToolTips_Click);
            // 
            // btnEditToolTip
            // 
            this.btnEditToolTip.Location = new System.Drawing.Point(15, 245);
            this.btnEditToolTip.Name = "btnEditToolTip";
            this.btnEditToolTip.Size = new System.Drawing.Size(103, 23);
            this.btnEditToolTip.TabIndex = 12;
            this.btnEditToolTip.Text = "Edit Tool Tip";
            this.btnEditToolTip.UseVisualStyleBackColor = true;
            this.btnEditToolTip.Click += new System.EventHandler(this.btnEditToolTip_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 439);
            this.Controls.Add(this.btnEditToolTip);
            this.Controls.Add(this.btnDestroyAllToolTips);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnHideAllToolTips);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCreateToolTip);
            this.Controls.Add(this.txtText);
            this.Controls.Add(this.txtY);
            this.Controls.Add(this.txtX);
            this.Controls.Add(this.lblText);
            this.Controls.Add(this.lblY);
            this.Controls.Add(this.lblX);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.TextBox txtText;
        private System.Windows.Forms.Button btnCreateToolTip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnHideAllToolTips;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnDestroyAllToolTips;
        private System.Windows.Forms.Button btnEditToolTip;
    }
}


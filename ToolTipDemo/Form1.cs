﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolTipDemo
{
    public partial class Form1 : Form
    {
        internal class ToolTipOptions
        {
            public ToolTipOptions(string text, int x, int y)
            {
                this.Text = text;
                this.X = x;
                this.Y = y;
            }

            public string Text { get; set; }
            public int X { get; set; }
            public int Y { get; set; }
        }

        private IList<ToolTipLib.ToolTip> ToolTips = new List<ToolTipLib.ToolTip>();

        public Form1()
        {
            InitializeComponent();
            this.txtX.Text = "0";
            this.txtY.Text = "0";
            this.txtText.Text = "enter some text here...";
        }

        private ToolTipOptions GetToolTipOptions()
        {
            string text = this.txtText.Text;
            int x = int.Parse(this.txtX.Text);
            int y = int.Parse(this.txtY.Text);

            return new ToolTipOptions(text, x, y);
        }

        private void btnCreateToolTip_Click(object sender, EventArgs e)
        {
            var options = GetToolTipOptions();
            var tooltip = new ToolTipLib.ToolTip();

            tooltip.Location = new Point(options.X, options.Y);
            tooltip.Text = options.Text;

            ToolTips.Add(tooltip);

            tooltip.Show();

        }

        private void btnDestroyAllToolTips_Click(object sender, EventArgs e)
        {
            foreach (var tooltip in this.ToolTips)
            {
                tooltip.Close();
            }

            this.ToolTips = new List<ToolTipLib.ToolTip>();
        }

        private void btnHideAllToolTips_Click(object sender, EventArgs e)
        {
            foreach (var tooltip in this.ToolTips)
            {
                tooltip.Hide();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var tooltip in this.ToolTips)
            {
                tooltip.Show();
            }
        }

        private void btnEditToolTip_Click(object sender, EventArgs e)
        {
            var options = GetToolTipOptions();
            foreach (var tooltip in this.ToolTips)
            {
                tooltip.Text = options.Text;
                tooltip.LocationX = options.X;
                tooltip.LocationY = options.Y;
            }
        }
    }
}

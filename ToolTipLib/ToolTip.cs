﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolTipLib
{
    public class ToolTip : Form
    {
        const int WS_EX_TOPMOST = 0x00000008;

        private DropShadow shadow;
        private Label label;

        public ToolTip()
        {
            this.SuspendLayout();

            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "";

            this.label = new Label();
            this.label.UseMnemonic = false;
            this.label.Top = 1;
            this.label.Left = 1;
            this.label.Name = "label";
            this.Text = "";

            this.FormBorderStyle = FormBorderStyle.None;
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.Padding = new Padding(3);
            this.StartPosition = FormStartPosition.Manual;
            this.ShowInTaskbar = false;
            this.Location = new Point(0, 0);
            this.Load += ToolTip_Load;
            this.AutoSizeChanged += ToolTip_AutoSizeChanged;

            this.ResumeLayout(false);
        }

        // http://stackoverflow.com/questions/6328962/form-without-focus-activation
        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        public override string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;
                this.label.Text = value;
                this.Invalidate();
                if (this.shadow != null)
                {
                    this.shadow.RefreshShadow();
                }
            }
        }

        public int LocationX
        {
            get { return this.Location.X; }
            set
            {
                var location = this.Location;
                location.X = value;
                this.Location = location;
            }
        }

        public int LocationY
        {
            get { return this.Location.Y; }
            set
            {
                var location = this.Location;
                location.Y = value;
                this.Location = location;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, ClientRectangle, Color.Black, ButtonBorderStyle.Solid);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                var createParams = base.CreateParams;
                createParams.ExStyle |= WS_EX_TOPMOST;
                return createParams;
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && this.shadow != null)
            {
                this.shadow.Dispose();
            }

            base.Dispose(disposing);
        }

        private void ToolTip_Load(object sender, EventArgs e)
        {
            shadow = new DropShadow(this)
            {
                ShadowBlur = 6,
                ShadowSpread = -3,
                ShadowH = 3,
                ShadowV = 3,
                ShadowColor = Color.FromArgb(150, 0, 0, 0)
            };

            label.Text = this.Text;
            label.AutoSize = true;
            this.Controls.Add(label);
            if (shadow != null)
            {
                shadow.RefreshShadow();
            }
        }

        private void ToolTip_AutoSizeChanged(object sender, EventArgs e)
        {
            // for when the form resizes
            ControlPaint.DrawBorder(this.CreateGraphics(), ClientRectangle, Color.Black, ButtonBorderStyle.Solid);

            shadow.RefreshShadow();
        }
    }
}
